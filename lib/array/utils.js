exports.flatten = function(array) {
	return [].concat(...array);
};

exports.mapBy = function(array, mapFunc) {
	const map = {};
	for (const element of array) {
		map[mapFunc(element)] = element;
	}
	return map;
};
