const fileHelper = require('@lib/io/file');
const path = require('path');
const { decrypt, encrypt, randomBytes } = require('@lib/sec/crypto');
const SALT_SIZE = 16;

const DEFAULT_CONFIG_FILE = path.resolve(process.cwd(), '.env.json');
const DEFAULT_PREFERENCE_FILE = path.resolve(process.cwd(), 'preference.json');

class ConfigHelper {
	readConfig(configFile = DEFAULT_CONFIG_FILE) {
		let { salt, username, password, jiraUrl } = JSON.parse(fileHelper.getContentSync(configFile));

		const secret = process.env.SECRET;
		salt = new Buffer(salt, 'hex');
		return { username: decrypt(username, secret, salt), password: decrypt(password, secret, salt), jiraUrl };
	}

	getConfig(configFile = DEFAULT_CONFIG_FILE) {
		if (!this.config) {
			this.config = this.readConfig(configFile);
		}

		return this.config;
	}

	getPreference({ preferenceFile = DEFAULT_PREFERENCE_FILE, section }) {
		if (!this.preference) {
			this.preference = JSON.parse(fileHelper.getContentSync(preferenceFile));
		}

		return this.preference[section];
	}

	writeConfig(config = this.config, configFile = DEFAULT_CONFIG_FILE) {
		const { username, password, jiraUrl, secret } = config;

		const salt = randomBytes(SALT_SIZE);

		fileHelper.writeContentSync(
			configFile,
			JSON.stringify({
				username: encrypt(username, secret, salt),
				password: encrypt(password, secret, salt),
				jiraUrl,
				salt: salt.toString('hex'),
			})
		);
	}
}

module.exports = new ConfigHelper();
