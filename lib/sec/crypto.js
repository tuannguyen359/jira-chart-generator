const crypto = require('crypto');
const ALGORITHM = 'aes-256-cbc';

exports.encrypt = function(plainText, secret, salt) {
	const hash = crypto.createHash('sha256');
	hash.update(salt);
	const key = hash.digest().slice(0, 32);
	const cipher = crypto.createCipheriv(ALGORITHM, key, salt);
	const encrypted = cipher.update(plainText, 'utf-8');
	return Buffer.concat([encrypted, cipher.final()]).toString('hex');
};

exports.decrypt = function(cipherText, secret, salt) {
	const hash = crypto.createHash('sha256');
	hash.update(salt);
	const key = hash.digest().slice(0, 32);
	const decipher = crypto.createDecipheriv(ALGORITHM, key, salt);
	const decrypted = decipher.update(cipherText, 'hex');
	return Buffer.concat([decrypted, decipher.final()]).toString('utf-8');
};

exports.randomBytes = function(size) {
	return crypto.randomBytes(size);
};
