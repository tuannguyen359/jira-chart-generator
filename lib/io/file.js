const util = require('util');
const fs = require('fs');
const readFile = util.promisify(fs.readFile.bind(fs));
const writeFile = util.promisify(fs.writeFile.bind(fs));

exports.getContentSync = function (file, options = {encoding: 'utf8'}) {
	return fs.readFileSync(file, options);
};

exports.getContent = async function (file, options = {encoding: 'utf8'}) {
	return await readFile(file, options);
};

exports.writeContentSync = function (file, content, options = {encoding: 'utf8'}) {
	return fs.writeFileSync(file, content, options);
};

exports.writeContent = function (file, content, options = {encoding: 'utf8'}) {
	return writeFile(file, content, options);
};
