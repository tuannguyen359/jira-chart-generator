require('module-alias/register');
const inquirer = require('inquirer');
const configHelper = require('./lib/core/config');

inquirer
	.prompt([
		{
			type: 'input',
			name: 'secret',
			message: 'Secret',
		},
		{
			type: 'input',
			name: 'username',
			message: 'Jira username',
		},
		{
			type: 'password',
			name: 'password',
			message: 'Jira password',
		},
		{
			type: 'input',
			name: 'jiraUrl',
			message: 'Jira URL',
			default: 'https://affinitylive.jira.com',
		},
	])
	.then(({ username, password, jiraUrl, secret }) => {
		const config = { username, password, jiraUrl, secret };
		configHelper.writeConfig(config);
		console.info('Initialised config');
	});
