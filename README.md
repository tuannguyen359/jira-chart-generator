## JIRA Chart generator

#### Setup
```
npm run setup
```
Enter Jira's username, password, and secret. The username and password will be encrypted using the secret. You must remember the secret to run the app

#### Run the app

Inline
```
"$(npm bin)"/cross-env SECRET=<secret> npm start
```

Export as a variable

```
export SECRET=<secret>
npm start
```

Go to localhost:8080 to view the app