module.exports = {
	extends: ['eslint:recommended', 'prettier'], // extending recommended config and config derived from eslint-config-prettier
	plugins: ['prettier'], // activating eslint-plugin-prettier (--fix stuff)
	rules: {
		'prettier/prettier': [
			// customizing prettier rules (unfortunately not many of them are customizable)
			'error',
			{
				singleQuote: true,
				trailingComma: 'es5',
			},
		],
		eqeqeq: ['error', 'always'], // adding some custom ESLint rules
		'no-console': ['error', { allow: ['error', 'warn', 'info'] }],
		'no-await-in-loop': ['error'],
	},
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 9,
	},
	globals: {
		c3: true,
		dayjs: true,
	},
	env: {
		commonjs: true,
		es6: true,
		node: true,
		jest: true,
		browser: true,
	},
};
