const http = require('./http');
const configHelper = require('@lib/core/config');

exports.getActiveSprints = function() {
	return http
		.get('/board/3/sprint', {
			params: {
				state: 'active',
			},
		})
		.then(({ data: { values } }) => values);
};

exports.getFutureSprints = function() {
	const jiraPrefs = configHelper.getPreference({ section: 'jira' });
	const excludedSprints = jiraPrefs.futureSprint.exclude || [];

	return http
		.get('/board/3/sprint', {
			params: {
				state: 'future',
			},
		})
		.then(({ data: { values } }) => values.filter(sprint => !excludedSprints.includes(sprint.name)));
};
