const Issue = require('@server/model/jira/issue');
const http = require('./http');
const MAX_RESULTS = 50;
const { flatten } = require('@lib/array/utils');
const dayjs = require('@lib/date/day');
const configHelper = require('@lib/core/config');
const preference = configHelper.getPreference({ section: 'jira' });

exports.getIssuesForSprint = async function({ sprintId, pageNumber }) {
	function getIssueForPage(pageNumber) {
		return http
			.get(`/board/3/sprint/${sprintId}/issue`, {
				params: {
					expand: 'changelog',
					fields: 'status,summary,created',
					maxResults: MAX_RESULTS,
					startAt: pageNumber * MAX_RESULTS,
				},
			})
			.then(({ data: { startAt, total, maxResults, issues } }) => ({
				startAt,
				total,
				maxResults,
				issues: issues.map(getIssuePayload),
			}));
	}
	if (pageNumber !== undefined) {
		return getIssueForPage(pageNumber).then(({ issues }) => issues);
	} else {
		const firstPage = await getIssueForPage(0);
		const hasMore = (firstPage.startAt + 1) * MAX_RESULTS < firstPage.total;

		let otherPages = [];

		if (hasMore) {
			const numPage = Math.ceil(firstPage.total / MAX_RESULTS);

			otherPages = await Promise.all(Array.from({ length: numPage - 1 }, (value, key) => key + 1).map(getIssueForPage));
		}

		const allPages = [firstPage, ...otherPages];

		return flatten(allPages.map(({ issues }) => issues));
	}
};

function getIssuePayload({ id, key, changelog: { histories = [] } = {}, fields: { status = {}, summary, created } }) {
	const statusHistory = histories
		.map(({ created, items }) => ({
			created,
			statusChange: items.filter(({ field }) => field === 'status')[0],
		}))
		.filter(({ statusChange }) => statusChange)
		.map(({ created, statusChange: { fromString, toString } }) => ({
			from: getStatus(fromString),
			to: getStatus(toString),
			changeAt: dayjs(created),
		}));

	const lastSprintChange = histories.filter(({ items }) => {
		return items.some(({ field, from, to }) => (to || from) && field === 'Sprint');
	})[0];

	const lastTimeInSprint = lastSprintChange && dayjs(lastSprintChange.created);

	return new Issue({
		id,
		key,
		created: dayjs(created),
		lastTimeInSprint,
		statusHistory,
		status: getStatus(status.name),
		statusCategory: getStatus(status.statusCategory.name),
		summary,
	});
}

function getStatus(status) {
	return preference.statusMapping[status] || status;
}
