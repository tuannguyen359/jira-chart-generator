const { default: axios } = require('axios');
const configHelper = require('@lib/core/config');
const config = configHelper.getConfig();
const axiosRetry = require('axios-retry');
const { setupCache } = require('axios-cache-adapter');

const CACHE_DEFAULT = 60 * 60 * 1000;

const cache = setupCache({
	maxAge: CACHE_DEFAULT,
	exclude: { query: false },
});

const instance = axios.create({
	baseURL: `${config.jiraUrl}/rest/agile/1.0`,
	auth: {
		username: config.username,
		password: config.password,
	},
	adapter: cache.adapter,
});

axiosRetry(instance, {
	retries: 3,
	retryCondition: error => axiosRetry.isNetworkOrIdempotentRequestError(error),
});

module.exports = instance;
