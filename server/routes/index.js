const express = require('express');
const statsController = require('@server/controllers/stats');
const catchErrors = require('@server/core/error-handlers').catchErrors;

const router = new express.Router();

router.get('/stats', catchErrors(statsController.getStats));

module.exports = router;
