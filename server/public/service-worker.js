var CACHE_VERSION = 1;
var CURRENT_CACHES = {
	prefetch: 'prefetch-cache-v' + CACHE_VERSION,
};

self.addEventListener('install', function(event) {
	var urlsToPrefetch = [
		'https://cdnjs.cloudflare.com/ajax/libs/d3/5.9.1/d3.min.js',
		'https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.13/c3.min.css',
		'https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.13/c3.min.js',
		'https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.10/dayjs.min.js',
	];

	event.waitUntil(
		caches
			.open(CURRENT_CACHES.prefetch)
			.then(function(cache) {
				var cachePromises = urlsToPrefetch.map(function(urlToPrefetch) {
					var url = new URL(urlToPrefetch, location.href);
					var request = new Request(url, { mode: 'no-cors' });
					return fetch(request)
						.then(function(response) {
							if (response.status >= 400) {
								throw new Error('request for ' + urlToPrefetch + ' failed with status ' + response.statusText);
							}
							return cache.put(urlToPrefetch, response);
						})
						.catch(function(error) {
							console.error('Not caching ' + urlToPrefetch + ' due to ' + error);
						});
				});

				return Promise.all(cachePromises).then(function() {
					console.info('Pre-fetching complete.');
				});
			})
			.catch(function(error) {
				console.error('Pre-fetching failed:', error);
			})
	);
});

self.addEventListener('activate', function(event) {
	var expectedCacheNames = Object.keys(CURRENT_CACHES).map(function(key) {
		return CURRENT_CACHES[key];
	});

	event.waitUntil(
		caches.keys().then(function(cacheNames) {
			return Promise.all(
				cacheNames.map(function(cacheName) {
					if (expectedCacheNames.indexOf(cacheName) === -1) {
						console.info('Deleting out of date cache:', cacheName);
						return caches.delete(cacheName);
					}
				})
			);
		})
	);
});

self.addEventListener('fetch', function(event) {
	event.respondWith(
		caches.match(event.request).then(function(response) {
			if (response) {
				return response;
			}

			return fetch(event.request)
				.then(function(response) {
					return response;
				})
				.catch(function(error) {
					throw error;
				});
		})
	);
});
