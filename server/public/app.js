const commonChartConfig = {
	padding: {
		top: 20,
		bottom: 20,
	},
	axis: {
		x: {
			type: 'category',
		},
	},
};

fetch('/api/stats')
	.then(res => res.json())
	.then(({ statuses, stats }) => {
		document.querySelector('.loader').style.display = 'none';
		let filteredStats = { ...stats };
		Object.keys(filteredStats).forEach(date => {
			if (!Object.keys(filteredStats[date]).length) {
				delete filteredStats[date];
			}
		});
		generateBarChart({ statuses, stats: filteredStats });
		generateLineChart({ statuses, stats: filteredStats });
		generateAreaChart({ statuses, stats: filteredStats });
		generateTable({ statuses, stats: filteredStats });
	});

function generateTable({ statuses, stats }) {
	const { categories, columns } = prepareData({ statuses, stats });
	const tableContainer = document.getElementById('table-container');
	tableContainer.innerHTML = '';

	const table = element('table');
	table.classList = 'paleBlueRows';
	const thead = element('thead');
	const tbody = element('tbody');
	table.append(thead, tbody);

	thead.appendChild(element('th'));

	for (const column of columns) {
		const columnName = column[0];
		const th = element('th');
		th.appendChild(text(columnName));

		thead.append(th);
	}

	categories.forEach((category, i) => {
		const tr = element('tr');
		tbody.append(tr);
		const categoryTd = element('td');
		categoryTd.appendChild(text(category));
		tr.append(categoryTd);
		for (const column of columns) {
			const td = element('td');
			td.appendChild(text(column[i + 1] || 0));
			tr.append(td);
		}
	});

	tableContainer.append(table);
}

function element(name) {
	return document.createElement(name);
}

function text(text) {
	return document.createTextNode(text);
}

function generateBarChart({ statuses, stats }) {
	const config = {
		...commonChartConfig,
		data: {
			type: 'bar',
			labels: true,
		},
		bindto: '#bar',
		bar: {
			width: {
				ratio: 0.75,
			},
		},
	};
	const { categories, columns } = prepareData({ statuses, stats });
	config.axis.x.categories = categories;
	config.data.columns = columns;
	c3.generate(config);
}

function generateLineChart({ statuses, stats }) {
	const config = {
		...commonChartConfig,
		data: {
			type: 'line',
		},
		bindto: '#line',
	};
	const { categories, columns } = prepareData({ statuses, stats });
	config.axis.x.categories = categories;
	config.data.columns = columns;
	c3.generate(config);
}

function generateAreaChart({ statuses, stats }) {
	const config = {
		...commonChartConfig,
		data: {
			type: 'area',
		},
		point: {
			show: false,
		},
		bindto: '#area',
	};
	const { categories, columns } = prepareData({ statuses, stats });
	config.axis.x.categories = categories;
	config.data.columns = columns;
	config.data.groups = [statuses];
	c3.generate(config);
}

function prepareData({ stats, statuses }) {
	const days = Object.keys(stats).sort();
	return {
		categories: days.map(day => {
			const dayObject = dayjs(day);
			if (dayObject.day() === 0) {
				return 'Start';
			}
			return dayObject.format('dddd');
		}),
		columns: statuses.map(status => {
			const dataPoints = days.map(day => {
				const dayStats = stats[day];
				return dayStats[status] === 0 || dayStats[status] === undefined ? null : dayStats[status];
			});
			return [status, ...dataPoints];
		}),
	};
}
