const { getIssuesForSprint } = require('../api/jira/issue');
const { getActiveSprints, getFutureSprints } = require('@server/api/jira/sprint');
const props = require('@lib/promise/props');
const dayjs = require('@lib/date/day');
const config = require('@lib/core/config');
const preference = config.getPreference({ section: 'jira' });

exports.getStats = async function(req, res) {
	const [activeSprints, futureSprints] = await Promise.all([getActiveSprints(), getFutureSprints()]);
	const allPromises = {};
	for (const sprint of [...activeSprints, ...futureSprints]) {
		allPromises[sprint.id] = getIssuesForSprint({ sprintId: sprint.id });
	}
	const issuesMapBySprintId = await props(allPromises);

	const issueStats = {};

	let today = dayjs();
	if (dayjs().day() === 1) {
		today = dayjs().add(-1, 'day');
	}
	const mondayOfThisWeek = today.startOf('week').startOf('day');

	const possibleStatuses = new Set();

	Array.from({ length: 6 })
		.map((k, v) => v)
		.forEach(index => {
			const day = mondayOfThisWeek.add(index, 'day');
			const dateString = day.add(-1, 'day').format('YYYY-MM-DD');
			issueStats[dateString] = issueStats[dateString] || {};
			for (const sprint of activeSprints) {
				const issues = issuesMapBySprintId[sprint.id];

				issues.forEach(issue => {
					const status = issue.getStatusInDay(day.toDate());
					if (!status) {
						return;
					}
					if (index && issue.lastTimeInSprint && day.isBefore(issue.lastTimeInSprint, 'day')) {
						return;
					}
					possibleStatuses.add(status);
					issueStats[dateString][status] = (issueStats[dateString][status] || 0) + 1;
				});
			}
			for (const sprint of futureSprints) {
				const issues = issuesMapBySprintId[sprint.id];
				issues.forEach(issue => {
					const status = issue.getStatusInDay(day.toDate());
					if (!status || status === 'Closed') {
						return;
					}
					if (issue.lastTimeInSprint && day.isBefore(issue.lastTimeInSprint, 'day')) {
						return;
					}
					possibleStatuses.add('Next Sprint');
					issueStats[dateString]['Next Sprint'] = (issueStats[dateString]['Next Sprint'] || 0) + 1;
				});
			}
		});

	const sortPreference = preference.order;

	res.json({
		statuses: Array.from(possibleStatuses).sort((a, b) => sortPreference.indexOf(a) - sortPreference.indexOf(b)),
		stats: issueStats,
	});
};
