const dayjs = require('@lib/date/day');
const today = dayjs().startOf('day');
module.exports = class Issue {
	constructor({ id, key, summary, status, created, statusCategory, statusHistory = [], lastTimeInSprint }) {
		this.id = id;
		this.key = key;
		this.status = status;
		this.statusCategory = statusCategory;
		this.statusHistory = statusHistory;
		this.summary = summary;
		this.created = created;
		this.lastTimeInSprint = lastTimeInSprint;
	}

	getStatusInDay(date) {
		const dateObject = dayjs(date);
		if (dateObject.isAfter(today, 'day') || dateObject.isBefore(this.created, 'day')) {
			return null;
		}
		const statusChange = this.statusHistory.find(({ changeAt }) => changeAt.isBefore(dateObject, 'day'));
		if (!statusChange) {
			return this.status;
		}
		return statusChange.to;
	}
};
