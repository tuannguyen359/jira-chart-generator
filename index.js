require('module-alias/register');
const express = require('express');
const app = express();
const apiRoutes = require('./server/routes');
const path = require('path');
const morgan = require('morgan');
const errorHandlers = require('./server/core/error-handlers');
const isDev = app.get('env') === 'development';

app.use(express.static(path.resolve('server', 'public')));
app.use(morgan(isDev ? 'dev' : 'combined'));
app.use('/api/', apiRoutes);
app.get('/*', (req, res) => {
	res.sendFile(path.resolve('server', 'public', 'index.html'));
});

if (isDev) {
	app.use(errorHandlers.developmentErrors);
}

app.use(errorHandlers.productionErrors);

const port = process.env.PORT || 8080;
app.listen(port, () => console.info(`Express started on port ${port}`));
